# SQL Overview
(To ME) query for RDBMS

[SQL at wikipedia](https://en.wikipedia.org/wiki/SQL)

-> History ;initially called SEQUEL

-> Syntax ; statement consists of clauses like update, set, where, ...

-> Standardization history ; Database Language SQL

-> Procedural extensions ; PL/SQL to add procedural programming language functionality


# SQL meaning analysis
Structured   <- adjective

Query        <- A query is a question, especially one that you ask an organization, publication, or expert.

Language

I think, SQL is key mapped with 'Execution Plan' of DB


# SQL Processing

[overview of SQL Processing](https://docs.oracle.com/en/database/oracle/oracle-database/12.2/cncpt/sql.html#GUID-0E8DFE97-F2DD-4183-8A4B-1EA9087E9E37)

[Optimizer Components](https://docs.oracle.com/en/database/oracle/oracle-database/12.2/cncpt/sql.html#GUID-D4940716-5F75-43CD-B8B4-45E6BCE5F8EF)
 
parse -> bind -> execute -> fetch


# Categories of SQL
Data Definition Languague(DDL) : CREATE, ALTER, DROP, RENAME, TRUCATE, COMMENT

Data Manipulation Language(DML)    : INSERT, UPDATE, DELETE, MERGE, SELECT 

Data Control Language(DCL)    : GRANT, REVOKE

Transaction Control Language(TCL)    : COMMIT, ROLLBACK, SAVEPOINT
, Transaction : DML문의 집합!

참고로, DDL, DCL은 implicit commit을 수행한다. 즉, 바로 DB에 반영된다.

!! DML의 INSERT의 경우, 타당하다면, commit을 해야 다른 사용자가 볼 수 있다.

## DDL : primary of database objects

```
data description language (DDL) is a syntax for creating and modifying database objects such as tables, indices, and users.
DDL statements are similar to a computer programming language for defining data structures, especially database schemas.

```
[DDL at Wikipedia](https://en.wikipedia.org/wiki/Data_definition_language)

database schema canbe data structures in a broad sense.

furthermore, database schema is collections of database objects [doc of Oracle](https://docs.oracle.com/cd/B19306_01/server.102/b14196/schema.htm#ADMQS008)

#### Database objects

우선, 테이블, 인덱스, 뷰, 시퀀스 시노님, 프로시저, 함수, 패키지 등.
 오라클의 객체는 자바의 객체와 무관하게 시작한 개념이다. 

#### Identifier
(To ME) the optimized key to access table instance or system

DDL의 create table 시 가장 중요한 핵심,식별자.
 가장 최소조건으로 테이블 인스턴스 혹은 열들을 구분할 수 있는 것. 가령 기본키, 외래키, 복합키

기본키 : 식별자, 그리고 보조 키 또는 후보 키 중 선택된것

외래키 : 식별자, 다른 테이블의 식별자이면서, 현재 테이블에 들어와있는 것

복합키 : 식별자, 기본키와 외래키 등의 조합을 통한 키

##### Extra
오라클의 경우, 다른 DB에서 varchar과 동일한 것이 varchar2이고, 오라클의 varchar는 별도의 계획으로 떼어놨다고한다. 용량이 더 적다.
 이런 부분을 create table 시 참고할 수 있다.
 
##### Cofigure learning environment
[Install Oracle at Windows](https://bitbucket.org/devsactiorigin/database/src/main/DoitOracle/Intro/InstallOracle_Win/)