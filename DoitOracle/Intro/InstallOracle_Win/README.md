# Install and config Oracle's tools at Windows

## overview
1.Install OracleXE184_Win64 + (SQL*PLUS || SQLDevolper)

2.Install sqldeveloper

3.Install datamodeler

## OracleXE184_Win64
OracleXE184_Win64.zip 알집풀고, setup.exe 실행, 기본값들로 설치 진행 후 설치완료, 윈도우용 파일로서 AWS EC2 Linux에는 못쓴다.

오라클 11g 이전까지는 DB들 간의 관계가 수평적이다. 12c부터는 집주인 DB와 세입자 DB로 구분하였고, 세입자 DB가 주로 클라이언트가 붙는곳, DBA가 아닌이상.

그리고 이러한 테넌트 DB의 대표적인 이름이 XEPDB1

* 설치 시 요구하는 password는 학습상 oracle로 설정, 실무에서는 중대보안사안

```
(설치 중 안내문 중 일부)
다중 테넌트 컨테이너 데이터베이스 : localhost:1521

플러그인할 수 있는 데이터베이스 : localhost:1521/XEPDB1

EM Express URL: https://localhost:5500/em
```

한편, 12c 이상 부터 업데이트 시 이름의 변경이지 큰 틀은 12c부터 21c까지 거의 유사하다고 한다.

이때 가장 주요한 기능은 Identity라고 채번칼럼에 응용가능한 자동 숫자 증가 칼럼을 지원

#### additional configuration
```
C:\app\COM\product\18.0.0\oradata\XE
```
상단 경로로 들어가면 DB의 파일들이 보인다. 이것은 집주인 DB이고, XEPDB1 폴더에 들어가면 내가 주로 접근할 DB파일들이 보인다.

한편, powershell같은 cmd 상에서 오라클 서비스를 껐다키는 학습을 위해

하단 윈도우 검색창에서 '서비스' 검색, 그리고 Oracle 관련 모든 서비스들은 수동으로 바꾼다. 

```
OracleJobSchedulerXE

OracleOraDB18Home1MTSRecoveryService

OracleOraDB18Home1TNSListener

OracleRemExecServiceV2

OracleServiceXE
```

## 오라클 서버 관리
net start ~, net stop ~ 방식으로 끄고 키기(powershell 시 관리자 권한 추천)
참고로 리스너를 먼저키고, 그다음 서버 서비스를 킨다. stop 시에도 이 순서를 지킨다.

```
net start OracleOraDB18Home1TNSListener
net start OracleServiceXE
```

#### 서버 onoff batch파일
Windows에 맞는 .bat 배치파일을 만든다. 

notepad xe_start.bat

=>
```
net start OracleOraDB18Home1TNSListener
net start OracleServiceXE
```
notepad xe_stop.bat

=>
```
net stop OracleOraDB18Home1TNSListener
net stop OracleServiceXE
```
*net : 윈도우 서버관리 명령어

배치파일이 존재하는 폴더에서 powershell로 아래와 같은 명령어로 배치파일을 실행가능하다.
```
$ start xe_stop
```
그리고 리눅스처럼 '$./xe_start' 방식도 가능하다.
 
## Client Tool : Access to Oracle
### SQL*PLUS  
SQL*PLUS is basic given CUI to access DB
[goto](https://bitbucket.org/devsactiorigin/database/src/main/InstallOracle_Win/SQLPLUS.md)

### SQL Developer
SQL Developer is GUI to access DB
[goto](https://bitbucket.org/devsactiorigin/database/src/main/InstallOracle_Win/SQLDeveloper.md)

## alternatives of oracle server
[LiveSQL](https://bitbucket.org/devsactiorigin/database/src/main/DoitOracle/Intro/InstallOracle_Win/alternatives/LiveSQL/)

[OracleATPDatabase](https://bitbucket.org/devsactiorigin/database/src/main/DoitOracle/Intro/InstallOracle_Win/alternatives/OracleATPDatabase/)