## SQL*PLUS
powershell 같은 CUI에서 DB접근 및 제어하는 클라이언트 툴

## 관리자 또는 세입자 계정으로 접근하기
앞선 .bat파일을 통해 리스너와 서버 서비스가 정상작동중인 상황을 가정한다. 아래 예시로서, 경로나 계정 등은 로컬환경마다 다를 수 있다.
또한, 빠른 적용을 위해 명령문 앞 $나 SQL>는 생략한다.

Container Databases (CDB)에 접속 ; 주인(sys) 컨테이너에 접속
```
sqlplus / as sysdba
```

한번에 세입자(system)로 접속,sys 는 sytem과 다르다!, 혹은 sys로 접속 후 전환
```
sqlplus system/oracle@localhost:1521/xepdb1

or

sqlplus / as sysdba
conn system/oracle@localhost:1521/xepdb1
```
(system은 사용자 이름, oracle은 password, 그리고 위치는 xepdb1) ; password is "oracle"

## 테이블 스페이스 생성

```
create tablespace apps datafile 'C:\APP\devsacti\PRODUCT\18.0.0\ORADATA\XE\XEPDB1\APPS01.DBF' size 100m;
select tablespace_name, file_name from dba_data_files;
```

### 출력 관련 세부 설정
```
col USERNAME  format a30;
col ACCOUNT_STATUS format a30;

select username, account_status
from dba_users
order by username;
```

## 계정 생성 및 권한 주기

혹시 모를 기존 유저와 cascade를 통해 연관 자료도 삭제, 새로 만드는데, 한번에 실수로 다 버릴까봐, 테이블스페이스를 분할한다.
```
(ace 계정)
drop user ace cascade;

create user   ace
	   identified by me
	   default tablespace users
	   temporary tablespace temp
	   quota 10m on users
	   quota 10m on apps;

(계정 권한 부여; 이것이 진행되야 sql developer에서 커넥션 생성가능)

grant create session, 
	  create table,
	  create procedure,
	  create trigger,
	  create view,
	  create sequence
	  to ace;
```

* hr, aoa 계정 생성 및 권한 부여과정도 위와같다.
(생성 후 체크)
conn hr/me@localhost:1521/xepdb1

## 샘플 테이블 및 데이터 생성
SQL> 는 생략됨
```
sqlplus ace/me@localhost:1521/xepdb1

(오라클 대표 계정들의 객체들_scott,hr_ 계정의 객체들)
@C:\Users\KOSA\Desktop\demobld.sql
@c:\dev\query\demobld.sql

@C:\Users\KOSA\Desktop\creobjects.sql
@c:\dev\query\creobjects.sql

set pagesize 100
col tname format a30
select * from tab;

set linesize 200
select * from emp;

set linesize 400
select * from employees;
exit

```

## 테이블 삭제 및 복구
SQL> 는 생략됨
```
sqlplus ace/me@localhost:1521/xepdb1

drop table books purge;
create table books(no number);

drop table books;

col tname format a40
select * from tab;

show recyclebin
select * from "BIN$VjqGNINvTkqowOmynrCoFg==$0";

flashback table books to before drop;
select * from tab;

drop table books;
show recyclebin
purge recyclebin;
show recyclebin
```

drop만 하면 테이블을 휴지통으로 이동시킨 것이고,완전 삭제를 위해선 purge recyclebin 추가 필요하며,
이때 내가 테이블 생성 시 정의한 테이블명이 ORIGINAL NAME이고, 따로 RECYCLEBIN NAME이 생성된 것을 show recyclebin을 통해 확인가능하다.

drop 시 purge 붙이면 완전 삭제이며 휴지통에서 다시 들고오는 것이 flashback

#### select를 통한 필요 명령어 만들기
계정은 삭제하지 않고 테이블만 모두 삭제하는 cmd 만들기
```
select 'drop table "'||tname||'" cascade constraints purge;' as cmd 
from tab;

select 'drop table '||tname||' cascade constraints purge;' as cmd 
from tab;
```
쌍따옴표 유무의 차이는 직접실행을 통해 확인해본다.