# sqldeveloper

sqldeveloper-20.4.0.379.2205-x64.zip 풀고, 실행한다.

이때, '이전 SQL Developer 설치에서 환경설정을 임포트하겠습니까?'라는 문구가 뜨는데, 예전에 썼던 config들을 승계할것인지 묻는것이다. 

나의 경우 해당사항이 없으므로 아니다 를 선택

## Making New Connections
창 좌측상단의 접속(초록색 +) 새로 클릭,

- 접속 이름   : ace@xepdb1  
- 사용자 이름 : ace
- 비밀번호    : me

- 호스트 이름 : localhost
- 포트        : 1521
- 서비스 이름 : xepdb1
		
참고로 상단은 대게 DBA가 제공하고 나는 사용만 한다.

### trial and error1 : hr 접속문제

https://jieun0113.tistory.com/72

아마도 계정은 만들고, 세션 연결 권한주는 부분을 빼먹어서 그런듯하다.

## Importing
SQL developer에서는 예제 파일_demobld.sql,creobjects.sql_을 임포트해서 실행 가능, 단 앞선 실습에서 ace 계정에는 해당 sql 속 테이블들이 이미존재하므로 다른 계정을 통해 테스트

csv파일이라도 임포팅가능하다.

만약 주어진 파일이 단순히 insert문의 모음이라면 어떻게할까? 그리고 칼럼명이 매우 길어서 creat table방식이 어렵다면?
특정 테이블에 별도로 넣기만 하는 것이라면, 아래와 같은 방식으로 기존이나 주어진 테이블의 칼럼만으로 구성된 테이블을 생성한 뒤 Insert를 한다.

```
create table mydept
as
select * from dept
where 1=2;

```

한편, 반대로 익스포트도가능

## waiting and client tool

여러개의 커넥션이 생기다가 초과되면, 대기가 생긴다.

## relationship on-off of Linster and server
리스너는 살아있는데, 서버가 죽은 경우 || 리스너가 죽은 경우

만약 ./xe_stop 을 하되 다시 Linster만 on하면 어떻게될까?

리스터는 살아있어서 응답은 하지만, 인스턴스는 죽어서 'xepdb1은 모르는 애'라고 응답한다.
이런 상황을 흔히 리스너는 살았는데, 서버는 죽어있다고 한다.

만약 리스너만 off하면?

리스너만 죽이면 그냥 찾지를 못한다. 왜냐하면 리스터를 중개해서 서버에 접근하기 때문이다.

결과적으로, 이렇게 리스너만 껐다키거나 하면 서로 못찾을때가 있어서 안정적으로 다 죽이고, 다 살리는 과정을 거친다.