# Oracle ATP Database
(To ME) not yet

[index of autonomous-database](https://docs.oracle.com/en/cloud/paas/autonomous-database/index.html)
[ATP database](https://positivemh.tistory.com/378)

## Sign up Oracle Cloud and get Instance Wallet to access
AWS와 마찬가지로 결제가 valid한 카드를 바탕으로 가입을 완료한다.

그리고 주어진 설정대로 DB 인스턴스를 생성한 후 'DB Connection' 버튼에서 접근을 위한 Instance Wallet을 발급받는다.

## Making Connection
SQL Developer를 바탕으로 새로운 접속을 만들되, 접속 유형을 '클라우드 전자지갑'으로 설정한다.

이를 통해서 별도의 엔드포인트 설정이나 포트 설정이 자동지원되는 것으로 보인다.

[sqlplus로 접근하기](https://positivemh.tistory.com/378)
!! 반디집말고 윈도우에 [unzip 명령어 추가](https://lee-mandu.tistory.com/530)해서 예제그대로 진행할것
그리고 export 대신 set도 가능

## Making user of ATP DB
```
CREATE USER new_user IDENTIFIED BY password;

GRANT CREATE SESSION TO new_user;
```
한편,default tablespace로는 DATA가 존재한다.

[Create Users on Autonomous Database - Connecting with a Client Tool](https://docs.oracle.com/en/cloud/paas/autonomous-database/adbsa/manage-users-create.html#GUID-54CA837B-CD6A-4ED2-A960-5874535818CB)

[Connecting SQL Developer to Autonomous Transaction Processing](https://www.oracle.com/webfolder/technetwork/tutorials/obe/cloud/atp/OBE_Connecting%20SQL%20Developer%20to%20Autonomous%20Transaction%20Processing/connecting_sql_developer_to_autonomous_transaction_processing.html)

[DATA 테이블 스페이스 할당량 변경](https://jack-of-all-trades.tistory.com/291)
