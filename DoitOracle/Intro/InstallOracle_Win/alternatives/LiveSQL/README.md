# Live SQL
practice environment given by Oracle

[goto](https://livesql.oracle.com/)

# Intro
Start Coding Now
=>좌측 상단 메뉴바, SQL Worksheet

=>SQL 하나씩 작성하고 실행 (Ctrl + Enter)

* ctrl+enter 한줄씩 실행

```
select user, sysdate from dual;
select * from v$version;
select * from tab;
select * from user_tables;
select * from all_tables;
```

# Schema
A schema is a collection of database objects [doc of Oracle](https://docs.oracle.com/cd/B19306_01/server.102/b14196/schema.htm#ADMQS008)

메뉴 바 Schema
=>Schema 드롭다운리스트 클릭

=>여러 스키마 확인

=>SQL Worksheet에서 SQL 수행

```
select owner, object_type, object_name
from all_objects
where owner in ('HR', 'SCOTT')
order by 1 desc, 2, 3;
```

*Owners ; AD, AV, CO, DD, SCOTT, HR, OLYM, OE, SH, WORLD 확인 가능

## EMP and DEPT(SCOTT)
select 'SELECT COUNT(*) FROM SCOTT.'||table_name||';' as commands
from all_tables
where owner = 'SCOTT';

## SCOTT Extensions
### Syntax help
jdbc 예제 체크 가능!!, 모든 테이블이 지원되는것으로 보이지는 않는다.

## Human Resources(HR)
select 'SELECT COUNT(*) FROM HR.'||table_name||';' as commands
from all_tables
where owner = 'HR';

*상단 과정을 통해서 미리 cmd를 저장해서 접근가능

(참고 : 다른 유저 테이블 조회 그리고 하단 아래쪽에 csv로 다운로드 가능!)
select * from scott.emp;
select * from hr.employees;

? object_type of column

# Help
기본 설명과 제약사항 체크

- Introduction

Your access to your assigned schema is temporary and the schema will be initialized and recycled for others
after a period of inactivity. To save your work permanently, you will need to save your session as a script. 
Saved scripts can be replayed, annotated, edited, shared and downloaded.

- ...

- Lockdown 파트 읽어보세요

Limitations
Components Not Available
Allowed CREATE Statements
Removed SQL Statements
Included PL/SQL Packages and Types
Data Object Limitations
Query Limitations
Data Dictionary Access


# Task 0 : My Scripts 만들기

아래 링크에서 SQL 다운로드 받아 My Scripts 만들어 보세요

https://www.oracletutorial.com/getting-started/oracle-sample-database/

* demobld.sql 활용, name은 임의설정가능.
=> select * from user_tables; 를 통해서 현재 USER(APEX_PUBLIC_USER), BONUS, DEPT, DUMMY,EMP,SALGRADE 테이블 확인가능

## Tasks at Code Library
Introduction to SQL

Aggregating Rows: Databases for Developers

Sorting and Limiting Rows: Databases for Developers

Joining Tables: Databases for Developers 

Union, Minus, and Intersect: Databases for Developers

Subqueries: Databases for Developers

Converting Rows to Columns and Back Again: Databases for Developers 

Analytic Functions: Databases for Developers

HOL Analytic Functions (Kscope17)

Real World Problem Solving with SQL

PL/SQL Anonymous Blocks

(추가로 performance 관련 검색을 통해서 tunning까지 학습가능)

## the other edu environment
https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all

https://www.hackerrank.com/domains/sql
