# Stored subprogram
(To ME) saved blocks

저장 서브프로그램은 ~.sql로 따로 들고다니는 것이 아니라, 오라클 서버에 저장한 블록이다

저장 서브프로그램은 정의에 따라 저장 프로시저, 저장 함수, 저장 패키지, 저장 트리거로 세분화된다.

## 익명 블록 VS 저장 서브프로그램
컴파일 횟수 

=> 실행마다 컴파일 vs 저장할 때 한 번 컴파일

공유 

=> 오라클 내 공유 불가 vs 오라클 내 접속자 간 공유 가능

다른 응용프로그램(like JDBC) 활용성 

=> 없음 vs 있음

## Implementations
block to Procedure, Function, Package, Trigger

#### case of Procedure
(TO ME) CRUD block + a

프로시저를 바탕으로 테이블에 대한 CRUD 프로시저를 생성하며 공유할 수 있다.

아래는 Create 기능의 프로시저 예시이다.
```
create or replace procedure myemp_insert
(p_no     myemp.empno%type, 
p_deptno myemp.deptno%type)
is
begin
insert into myemp(no, deptno)
values(p_no, p_deptno);
end;
/
```
#### case of Function
(TO ME) returning block + a
```
create or replace function get_myemp_deptno
(p_empno number)
return varchar2 
is
v_dname myemp.deptno%type;
begin
select deptno into v_dname
from myemp
where deptno = p_empno;

return v_dname;
end;
/

exec dbms_output.put_line(get_myemp_deptno(1001));
```
#### case of Package
(TO ME) comprehensive block that has procedure, function you need + a

it consists of 'package' and 'package body'

(sample of Packge for readability)
```
(package)
create or replace package t1_pack
is
procedure t1_insert
(p_col1     t1.col1%type, 
 p_col2     t1.col2%type,
 p_col3     t1.col3%type);

...

end;
/

(package body)
create or replace package body t1_pack
is

procedure t1_insert
(p_col1     t1.col1%type, 
 p_col2     t1.col2%type,
 p_col3     t1.col3%type)
is
begin
  ...
    insert into t1(col1, col2, col3)
    values(p_col1, p_col2, p_col3);
  ...
  commit;
end;

...

end;
/

```

#### case of Trigger
(TO ME) block catching event you want to monitor + a
```
CREATE OR REPLACE TRIGGER trg_emp_log
AFTER
INSERT OR UPDATE OR DELETE ON EMP_TRG
FOR EACH ROW

BEGIN

   IF INSERTING THEN
      INSERT INTO emp_trg_log
      VALUES ('EMP_TRG', 'INSERT', :new.empno,
               SYS_CONTEXT('USERENV', 'SESSION_USER'), sysdate);

   ELSIF UPDATING THEN
      INSERT INTO emp_trg_log
      VALUES ('EMP_TRG', 'UPDATE', :old.empno,
               SYS_CONTEXT('USERENV', 'SESSION_USER'), sysdate);

   ELSIF DELETING THEN
      INSERT INTO emp_trg_log
      VALUES ('EMP_TRG', 'DELETE', :old.empno,
               SYS_CONTEXT('USERENV', 'SESSION_USER'), sysdate);
   END IF;
END;
/
```