# 무작정 따라하기 
## unnamed block
```
set serveroutput on

begin
dbms_output.put_line('Hello World!, i am unnamed procedure');
end;
/
```
## named block
```
create or replace procedure p1
is
begin
dbms_output.put_line('Hello World!, i am named procedure,p1');
end;
/

exec p1

```
에러 시,
show errors

## Implementations
unnamed, named block
#### variable, parameter
```
set serveroutput on

-- variable, parameter
declare
v_end NUMBER(2) := 10;
v_farewell VARCHAR2(20) default 'after loop, goodbye';
begin
for i in 1..v_end loop
  dbms_output.put_line('Hello World! '|| i ||' times!');
end loop;

dbms_output.put_line(v_farewell);
end;
/

create or replace procedure p1(a number)
is
begin
for i in 1..a loop
  dbms_output.put_line('Hello World! '|| i ||' times!');
end loop;
end;
/

exec p1(100)
```
#### if ,mod, comment;-- , /**/
```
/*
for ~ loop -> end loof, if ~ then -> end if, furthermore

if (cond1) then ~ else ~ end if
if (cond1) then ~ elseif (cond2) elseif ~ else ~ end if

*/
create or replace procedure p1(a number)
is
begin
for i in 1..a loop
  if mod(i, 2) = 0 then
	dbms_output.put_line('Hello World! '|| i ||' times!');
  end if;
end loop;
end;
/

exec p1(100);
```

#### case, TRUNC
```
DECLARE
   V_SCORE NUMBER := 87;
BEGIN
   CASE
      WHEN V_SCORE >= 90 THEN DBMS_OUTPUT.PUT_LINE('A학점');
      WHEN V_SCORE >= 80 THEN DBMS_OUTPUT.PUT_LINE('B학점');
      WHEN V_SCORE >= 70 THEN DBMS_OUTPUT.PUT_LINE('C학점');
      WHEN V_SCORE >= 60 THEN DBMS_OUTPUT.PUT_LINE('D학점');
      ELSE DBMS_OUTPUT.PUT_LINE('F학점');
   END CASE;
END;
/

DECLARE
   V_SCORE NUMBER := 12.34;
   case1 NUMBER default TRUNC(V_SCORE);
BEGIN
   CASE TRUNC(V_SCORE/10)
      WHEN 10 THEN DBMS_OUTPUT.PUT_LINE('A학점');
      WHEN 9 THEN DBMS_OUTPUT.PUT_LINE('A학점');
      WHEN 8 THEN DBMS_OUTPUT.PUT_LINE('B학점');
      WHEN 7 THEN DBMS_OUTPUT.PUT_LINE('C학점');
      WHEN 6 THEN DBMS_OUTPUT.PUT_LINE('D학점');
      ELSE DBMS_OUTPUT.PUT_LINE('F학점');
   END CASE;
   
   dbms_output.put_line('case1 is '||case1);
   dbms_output.put_line(V_SCORE/10);
   dbms_output.put_line(TRUNC(V_SCORE/10));
   -- select TRUNC(V_SCORE),TRUNC(V_SCORE,1),TRUNC(V_SCORE,2),TRUNC(V_SCORE,-1),TRUNC(V_SCORE,-2) from dual;
   -- above line is required to write down 'into' but out of this block below select is possible let's look after
END;
/
SELECT TRUNC(115.155), TRUNC(115.155, 1), TRUNC(115.155, 2) FROM DUAL;
SELECT TRUNC(115.155, -2),TRUNC(115.155, -1) FROM DUAL;
```