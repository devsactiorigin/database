# chap 17 : 레코드와 컬렉션

## record
(To ME) oracle's row-oriented object corresponding to struct of C

c에서 보다 탄력적인 사용자 정의 자료형 지원을 위해 struct가 있듯이, 오라클에도 이러한 것이 레코드라고 생각한다.
 한편, java의 필드만 가진 객체도 이와 유사하다고 생각한다.
 
그리고 declare된 개별 struct나 class처럼, record도 1묶음만 저장할 수 있고, 테이블에 적용하면 1행이다.

이러한 레코드와 관련하여 ~%TYPE 방식이 쓰이는데, 이는 존재하는 테이블 칼럼의 자료형을 포인팅한다고 이해할 수 있다. 포인터처럼
 다만, 중요한 점은 그 칼럼의 모든 값을 사용하기 위해 칼럼의 자료형을 참조하는 것이 아니라, 이를 바탕으로 1행을 활용하기 위함이다.
 결과적으로 1행이 된다.

아래 방식와 같은 방식을 통해 생성된 1행이 목표 테이블_DEPT_RECORD_의 행에 추가된다고 볼 수 있다.
```
DECLARE
   TYPE REC_DEPT IS RECORD(
      deptno NUMBER(2) NOT NULL := 99,
      dname DEPT.DNAME%TYPE,
      loc DEPT.LOC%TYPE
   );
   -- below line make name of record as 'dept_rec'
   dept_rec REC_DEPT;
BEGIN1
   -- below way that define value of record one by one, but it also canbe defined by table that fit 'where ~'
   dept_rec.deptno := 99;
   dept_rec.dname := 'DATABASE';
   dept_rec.loc := 'SEOUL';

   INSERT INTO DEPT_RECORD
   VALUES dept_rec;
END;
/

SELECT * FROM DEPT_RECORD;
```

## collection
(To ME) oracle's column-oriented object corresponding to array of C

there are associative array, nested table, variable-size array

#### associative array(or index by table)
(To ME) array that corresponding to values of column

연관 배열이란 키-값으로 구성된 컬렉션이라고 한다. 이런 점에서 index by table이라고도 하는듯하다.

레코드를 통해서 테이블의 행단위로 접근하고 다른 곳에 이동시키는 작업을 했다면, 이에 대응하여 열 단위로 접근하고 작업을 할 수 있도록 해주는 것이 컬렉션이라고 생각한다.

그리고 목표 테이블의 모든 열마다 associative array를 정의하여, arrays를 설정할 수 있고, 이를 통해 '열'을 기준으로 테이블의 값을 조회할 수 있다.

이때 컬렉션과 관련하여 ~%ROWTYPE 방식이 쓰이는데, 이는 존재하는 테이블의 모든 칼럼들의 자료형들을 포인팅한다고 이해할 수 있다. 포인터처럼

이로 인해 '행 중심' 접근으로 오해하기 쉬우나, 이를 바탕으로 이 칼럼들이 가지는 모든 값들을 접근하기 위함이므로, 결과적으로 '열 중심' 접근을 위한 방식이라고 생각한다.

아래 방식와 컬렉션 방식을 통해 목표 테이블을 열의 관점에서 접근하고 출력한다고 생각한다.

```
   TYPE ITAB_DEPT IS TABLE OF DEPT%ROWTYPE
      INDEX BY PLS_INTEGER;

   dept_arr ITAB_DEPT;
   idx PLS_INTEGER := 0;

BEGIN
   /*below example can make you 'it would better to use 'select * from dept;'?
   but if you want to see only part of column of table, it is better or maybe only way
   */
   FOR i IN(SELECT * FROM DEPT) LOOP
      idx := idx + 1;
      dept_arr(idx).deptno := i.DEPTNO;
      dept_arr(idx).dname := i.DNAME;
      dept_arr(idx).loc := i.LOC;

      DBMS_OUTPUT.PUT_LINE(
      dept_arr(idx).deptno || ' : ' ||
      dept_arr(idx).dname || ' : ' ||
      dept_arr(idx).loc);
   END LOOP;
END;
/
```