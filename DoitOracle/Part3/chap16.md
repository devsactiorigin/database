# chap 16 : PL/SQL
(To ME) PL/SQL is Block which is Integration of SQL

Procedural Language for SQL 

= PL/SQL = SQL(manipulating power) + 3GL(processing power)

= Block Structured Language(= Anonymous(Unnamed) Block && Named Block ; Procedure, Function, Package, Trigger, Object, ...)

[Database PL/SQL Language Reference](https://docs.oracle.com/en/database/oracle/oracle-database/18/lnpls/index.html)

## unnamed block(=Anonymous Block)
```

    declare       -- optional    
      선언부
    begin         -- mandatory
      실행부
    exception     -- optional
      예외처리부
    end;          -- mandatory
	/
```
## named block
```
	create or replace (block_kind) (block_name)
    is
      선언부
    begin
      실행부
    exception
      예외처리부
    end;
    /
```
';'는 statement의 끝을 상징하지만 실행을 내포하지는 않음.
 SQL*PLUS에서는 '/'을 통해 블록의 끝이자 실행을 의미.
 그리고 블록 작성을 이를 벗어나는 방법은 '.'을 넣고 엔터

# Features and Architecture of PL/SQL
[Overview of PL/SQL](https://docs.oracle.com/en/database/oracle/oracle-database/18/lnpls/overview.html#GUID-2FBCFBBE-6B42-4DB8-83F3-55B63B75B1EB)

[sub-reference](https://docs.oracle.com/database/121/LNPLS/overview.htm#LNPLS001)

## Advantages of PL/SQL
[goto](https://docs.oracle.com/en/database/oracle/oracle-database/18/lnpls/overview.html#GUID-17166AA4-14DC-48A6-BE92-3FC758DAA940)

Tight Integration with SQL

High Performance

High Productivity

Portability

Scalability

Manageability

Support for Object-Oriented Programming

point1. PL/SQL은 오라클 고유기능이라 할수있으나, 오라클 생태계가 넓다는 점에서 portable하다.

point2. PL/SQL의 높은 성능으로 DBMS의 scalability(확장성, 또는 대규모처리능력을 통한 서비스능력)을 확보

## Main Features of PL/SQL
[goto](https://docs.oracle.com/en/database/oracle/oracle-database/18/lnpls/overview.html#GUID-66D23BD1-0489-43B0-8108-FA14CA579B52)

Error Handling

Blocks

Variables and Constants

Subprograms

Packages

Triggers

Input and Output

Data Abstraction

Control Statements

Conditional Compilation

Processing a Query Result Set One Row at a Time
## Architecture of PL/SQL
[goto](https://docs.oracle.com/en/database/oracle/oracle-database/18/lnpls/overview.html#GUID-6C7B2CED-969A-4861-9CDF-25FD804FAE6E)

PL/SQL Engine

PL/SQL Units and Compilation Parameters

## Implementations
[무작정 따라하기](https://bitbucket.org/devsactiorigin/database/src/main/DoitOracle/Part3/PLSQL1.md)