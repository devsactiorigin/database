# Database
(To ME) Database is domain of process to make Information range

Database is managed by DB Management System, DBMS like Oracle.
 OracleDB can be standard of DB, and Oracle can be standard of DBMS

and when User who was granted to input the valid Query to DBMS, the result of this act canbe called as 'User Data'.
 at the same time, DBMS make the corresponding data which is called as 'Meta Data' to deal with and control 'User Data'

## Classifying of Database
(To ME) categorize DBs as theirs datastructure or architecture to save data

#### Datastructure
DB
= Relational Database (SQL Database, Tabular Database) || Non-Relational Database (NoSQL Database)

= 자료구조(Table) + 입출력명령(SQL) || 자료구조(Table, 다양한 자료구조) + 입출력명령(SQL, 다양한 문법 명령)

(ex) Oracle, MariaDB, MySQL, SQLite || MongoDB, Redis(key-value), Cassandra,Neo4j(graph) ...   

####
 사용자 확보를 위해 주력 외에도 NoSQL은 tabular를, RDB는 다양한 자료구조를 지원하여 대체로 'Multi-model'로 확장된다.[goto](https://db-engines.com/en/ranking)
####
 RDB를 structured, NoSQL을 unstructured라고 분류하기도 했지만, key-value나 Graph도 자료구조이기 때문에 다소 모호성이 존재한다.
점차 table이 기준인 DB는 Tabular라는 표현을 쓰기 시작했다.

#### Architecture
(To ME) centralized || Distributed || fusion(=centralized && Distributed)

중앙형 데이터베이스는 대표적으로 1개의 DB인스터스를 생각할 수 있고, 대체로 송수신이 수초내의 실시간 서비스에 적합하다.
분산형 데이터베이스는 대표적으로 Hadoop, Spark가 있고 대체로 연구공간의 데이터분산용으로서, 실시간 서비스는 지향하지 않는다.(file System + 분석 기능 -> Hadoop, Spark)
융합 형태는 2가지의 장점과 다양한 DBMS들의 장점을 모두 사용하고자 한 형태로, 가령 서비스용 중앙형 DB 데이터를 분산데이터베이스에 이관하고 분석용으로 사용하는 경우이다. 즉, 오라클로 실시간 서비스, 하둡으로 분산처리, 마지막으로 몽고DB 같은걸로 다각적 접근을 가능하게 하는 경우이다.

이 때, 일반 개인 사용자도 이런 이면의 데이터베이스에 접근 가능한 경우가 있는데, 자주 접근하지 않는 서비스에 한하여, 그리고 READOnly만 하는 특수한 경우이다.
대표적으로(만약 그러하다면)네이버의 블로그 통계 관련 데이터 조회 서비스를 네이버 일반 유저가 사용하는 경우이다.

## Extensions
#### Independence from DBMS
 JPA 등의 특징으로서 어떤 데이터베이스이든 이식하고 적용할 수 있다는 점에서 범용성은 좋으나, 한편으로는 개별 DB들의 고유강점, 가령 oracle의 PLSQL_은 일부 타협했다고 할 수 있다.
일부 경우, 제한된 자원이 더욱 제한될 것으로 생각된다.

#### DataModeling
(To ME) selecting Data and config Access alongside the given purpose of DB

excel 또한 어느정도의 공유와 보안이 가능하지만, DB는 보다 광범위한 접근, 세밀한 보안설정, 안정적인 백업 등이 가능하다.

이러한 DB의 활용성을 극대화하기 위한 모델링을 DataModeling이라고 생각한다. 클라이언트 툴에 내장 데이터 모델러가 존재하나, datamodeler같은 독립적인 데이터모델러도 존재한다.

참고로 ~.dbf  와같은 DB 파일은 노출되는 것만으로 중대한 보안 사고이다

#### DB adnd DBA
(To be supplemented)

# Oracle (Database) Server = Database + Instance

Database = datafile + redo log file + control file

Instance = SGA + BGP

[Oracle Server Architecture](http://step2oraclehome.blogspot.com/2010/06/oracle-server-architecture.html)

+ Listener is paired with Server(Database+Instance)

## Instance
(To ME) Mean of Accessing DB to data

.doc파일은 Msword 프로그램,보다 정확히는 Instance,를 '경유해야만' 사람이 볼수있다. 이런 점에서 Instance를 Mean이라고도 한다고 한다.

이와 같이, orcl.dbf, prod.dbf_오라클 진영에서 일반적으로 쓰는 첫번째, 두번째 db 파일명_라는 오라클 파일이 존재할때, 각각의 파일을 접근하기 위해 개별 Instance가 생성되며,
 이때 (Instance1+ orcl.dbf) , (Instance2+ orcl.dbf)는 각각을 Oracle Server라고 일컫기도 한다.

#### feature of Instance
Instance명은 대게 이것이랑 연결되는 데이터베이스 파일명과 동일하게 해준다. 가령, (orcl + orcl.dbf)
 참고로, XE버전은 교육용으로 허용된 에디션이고, 이때는 위와같은 Server를 1개만 허용한다고 한다. 그리고 orcl이 아닌 XE로 통일되어있다고한다.

한편, 서버의 Listener(포트) 넘버는 1521로 초기설정되어있다고 한다. DBA들이 필요에 따라 추가할 수 있다고 한다.

# Access to Oracle Server
## Client Tool
Oracle Server 에 보다 쉽게 접근하고, 처리하는 것을 돕는 툴,(DB client Tool) 

가령, SQL*Plus(CUI) at powershell, SQL Developer(GUI)

## Lesson : reference + extension
### reference
[Do it! 오라클로 배우는 데이터베이스 입문](./DoitOracle/)
+ [related Github](https://github.com/GroovySunday/doit-oracle)

### extensions
[JDBC](https://bitbucket.org/devsactiorigin/JDBC/)